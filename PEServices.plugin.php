<?php
/*
Plugin Name: Protopia Ecosystem Remote services
Plugin URI: http://wp-ermak.ru/
Description: Protopia Ecosystem Remote services
Version: 0.0.1
Author: Genagl
Author URI: http://wp-ermak.ru/author
License: GPL2
Text Domain:   pesrv
Domain Path:   /lang/
*/
/*  Copyright 2018  Genagl  (email: genag1@list.ru)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/ 

//библиотека переводов
function init_textdomain_pesrv() 
{ 
	if (function_exists('load_plugin_textdomain')) 
	{
		load_plugin_textdomain("pesrv", false , dirname( plugin_basename( __FILE__ ) ) .'/lang/');     
	} 
}
add_action('plugins_loaded', 'init_textdomain_pesrv');

//Paths
define('PESRV_URLPATH', WP_PLUGIN_URL.'/pe_services/');
define('PESRV_REAL_PATH', WP_PLUGIN_DIR.'/'.plugin_basename(dirname(__FILE__)).'/');
define('_REAL_PATH', PESRV_REAL_PATH);
define('PE_REMOTE_TYPE', "pe_remote");
define('PE_REMOTE_ACCOUNT_TYPE', "pe_remote_account");


require_once(PESRV_REAL_PATH.'class/PEServices.class.php');
require_once(PESRV_REAL_PATH.'class/PERemote.class.php');
require_once(PESRV_REAL_PATH.'class/PERemoteAccount.class.php');


if (function_exists('register_activation_hook'))
{
	register_activation_hook( __FILE__, [ PEServices, 'activate' ] );
}

if (function_exists('register_deactivation_hook'))
{
	register_deactivation_hook(__FILE__, array(PEServices, 'deactivate'));
}




add_filter("init", function()
{
	PEServices::get_instance();
	PERemoteAccount::init();
	PERemote::init();
}, 2);

