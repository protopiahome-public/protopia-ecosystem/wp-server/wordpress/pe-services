<?php

class PEServices
{
	static $options;
	static $instance;
	static $errors;
	static function get_instance()
	{
		if(!static::$instance)
		{
			static::$instance = new static;
		}
		return static::$instance;
	}
	static function activate()
	{
		static::$options = get_option(PESRV);
		static::update_options();
	}
	static function deactivate()
	{
		
	}	
	static function update_options()
	{
		update_option(PESRV, static::$options);
	}
	function __construct()
	{
		static::$options = get_option( PESRV );		
		add_action( 'admin_menu',					[__CLASS__, 'admin_page_handler'], 9);
		add_filter( "smc_add_post_types",	 		[__CLASS__, "init_obj"], 10);
		add_action( 'admin_enqueue_scripts', 		[__CLASS__, 'add_admin_js_script'] );
		add_action( 'wp_enqueue_scripts', 			[__CLASS__, 'add_frons_js_script'], 2 );
	}
	
	static function admin_page_handler()
	{
		add_menu_page( 
			__('Protopia', PESRV), 
			__('Protopia', PESRV),
			'manage_options', 
			'pesrv_page', 
			[__CLASS__, 'setting_pages'], 
			PESRV_URLPATH."assets/img/robot_light.svg", // icon url  
			'2'
		);
	}
	static function setting_pages()
	{ 
		require_once PESRV_REAL_PATH . "tpl/admin.php" ;
	}
	
	
	static function add_admin_js_script()
	{	
		//css
		wp_register_style("bootstrap", PESRV_URLPATH . 'assets/css/bootstrap.min.css', array());
		wp_enqueue_style( "bootstrap");
		wp_register_style("formhelpers", PESRV_URLPATH . 'assets/css/bootstrap-formhelpers.min.css', array());
		wp_enqueue_style( "formhelpers");		
		wp_register_style("periodpicker", PESRV_URLPATH . 'assets/css/jquery.periodpicker.min.css', array());
		wp_enqueue_style( "periodpicker");
		wp_register_style("fontawesome", 'https://use.fontawesome.com/releases/v5.5.0/css/all.css', []);
		wp_enqueue_style( "fontawesome" );
		
		wp_register_style(PESRV, PESRV_URLPATH . 'assets/css/bio.css', array());
		wp_enqueue_style( PESRV);
					
		wp_register_script("bio_admin", plugins_url( '../assets/js/bio_admin.js', __FILE__ ), array());
		wp_enqueue_script("bio_admin");
		wp_register_script("bootstrap", PESRV_URLPATH . 'assets/js/bootstrap.min.js' , array());
		wp_enqueue_script("bootstrap");
		wp_register_script("datetimepicker", PESRV_URLPATH . 'assets/js/jquery.datetimepicker.js' , array());
		wp_enqueue_script("datetimepicker");
		
		// load media library scripts
		wp_enqueue_media();
		//ajax
		wp_localize_script( 
			'jquery', 
			'myajax', 
			array(
				'url' => admin_url('admin-ajax.php'),
				'nonce' => wp_create_nonce('myajax-nonce')
			)
		);	
		
		$yes = substr_count ($_SERVER["REQUEST_URI"],  "/wp-admin/post.php?post=") && $_GET['action'] == "edit";
		$yes = $yes || $_SERVER["REQUEST_URI"] == "/wp-admin/admin.php?page=pesrv_page";
		
		if( !$yes ) return;
			
			//js
			wp_register_script("bootstrap", plugins_url( '../js/bootstrap.min.js', __FILE__ ), array());
			wp_enqueue_script("bootstrap");
			wp_register_script("formhelpers", plugins_url( '../js/bootstrap-formhelpers.min.js', __FILE__ ), array());
			wp_enqueue_script("formhelpers");
			wp_register_script(PESRV, plugins_url( '../assets/js/bio.js', __FILE__ ), array());
			wp_enqueue_script(PESRV);
			
			// load media library scripts
			wp_enqueue_media();
			wp_enqueue_style( 'editor-buttons' );
	}
	
	static function init_obj($init_object)
	{
		if(!is_array($init_object)) $init_object = [];
		$point					= [];
		$point['t']				= ['type' => 'post'];	
		$point['class']			= ['type' => 'PERemoteAccount'];
		$point['url'] 			= ['type' => 'url',  "name" => __("Remote Service URL", BIO) ];
		$point['account_owner'] = ['type' => 'url',  "name" => __("Owner Account", BIO) ];
		$point['token'] 		= ['type' => 'string',  "name" => __("Asset token", BIO), "hidden" => true];
		$init_object[PE_REMOTE_ACCOUNT_TYPE]	= $point;
		
		
		$point					= [];
		$point['t']				= ['type' => 'taxonomy'];	
		$point['class']			= ['type' => 'PERemote'];		
		$point['title_descr']	= ['type' => "text", "name" => __("Title of description", BIO)];
		$point[PE_REMOTE_ACCOUNT_TYPE]	= [
			'type' 		=> "array",  
			'class'		=> "PERemoteAccount", 
			"object" 	=> PE_REMOTE_ACCOUNT_TYPE, 
			"name" 		=> __("Remote Account", BIO)
		];	
		$init_object[PE_REMOTE_TYPE]	= $point;

		return $init_object;
		
	}
	
}
	
	
	