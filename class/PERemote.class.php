<?php


require_once(__DIR__ . "/../../pe-graphql/vendor/autoload.php");

use GraphQL\Error\ClientAware;
use GraphQL\Utils\BuildSchema;
use GraphQL\Utils\SchemaPrinter;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;



class PERemote extends SMC_Taxonomy
{
	static $test_edit;
	static function get_type()
	{
		return PE_REMOTE_TYPE;
	}
	static function init()
	{
		add_action('init', 					[ __CLASS__, 'register_all' ], 22);
		add_action( 'parent_file',			[ __CLASS__, 'tax_menu_correction'], 20);	
		add_action( 'admin_menu', 			[ __CLASS__, 'tax_add_admin_menus'], 21);
		
		parent::init();
	}
	
	static function register_all()
	{
		register_taxonomy(
			static::get_type(), 
			[ PE_REMOTE_ACCOUNT_TYPE ], 
			array(
				'label'                 => '', // определяется параметром $labels->name
				'labels'                => array(
					'name'              => __("Remote", PESRV),
					'singular_name'     => __("Remote", PESRV),
					'search_items'      => __('search Remote', PESRV),
					'all_items'         => __('all Remotes', PESRV),
					'view_item '        => __('view Remote', PESRV),
					'parent_item'       => __('parent Remote', PESRV),
					'parent_item_colon' => __('parent Remote:', PESRV),
					'edit_item'         => __('edit Remote', PESRV),
					'update_item'       => __('update Remote', PESRV),
					'add_new_item'      => __('add new Remote', PESRV),
					'new_item_name'     => __('new Remote Name', PESRV),
					'menu_name'         => __('Remote', PESRV),
				),
				'description'           => '', // описание таксономии
				'public'                => true,
				'publicly_queryable'    => null, // равен аргументу public
				'show_in_nav_menus'     => true, // равен аргументу public
				'show_ui'               => true, // равен аргументу public
				'show_in_menu'          => true, // равен аргументу show_ui
				'show_in_nav_menus' 	=> true,
				'show_tagcloud'         => true, // равен аргументу show_ui
				'show_in_rest'          => null, // добавить в REST API
				'rest_base'             => null, // $taxonomy
				'hierarchical'          => false,
				'update_count_callback' => '',
				'rewrite'               => true,
				//'query_var'             => $taxonomy, // название параметра запроса
				'capabilities'          => array(),
				'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
				'show_admin_column'     => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
				'_builtin'              => false,
				'show_in_quick_edit'    => null, // по умолчанию значение show_ui
			) 
		);
	
	}
	static function tax_menu_correction($parent_file) 
	{
		global $current_screen;
		$taxonomy = $current_screen->taxonomy;
		if ( $taxonomy == static::get_type() )
			$parent_file = 'pesrv_page';
		return $parent_file;
	}
	static function tax_add_admin_menus() 
	{
		add_submenu_page( 
			'pesrv_page', 
			__("Remotes", PESRV), 
			__("Remotes", PESRV), 
			'manage_options', 
			'edit-tags.php?taxonomy=' . static::get_type()
		);
		add_meta_box( "add-{PE_REMOTE_TYPE}", __("Courses", PESRV), 'wp_nav_menu_item_taxonomy_meta_box', 'nav-menus', 'side', 'default', static::get_type() );	
    }
}
