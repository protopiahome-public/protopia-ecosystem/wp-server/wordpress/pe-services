<?php
function get_bio_cab_plate()
{
	$html = "
		<div class='row'>
			<div class='col-12'>";
	$user 	= wp_get_current_user();
	$ro		= "<ul>";
	foreach((array) $user->roles as $role)
	{
		foreach(get_full_bio_roles() as $rr)
		{
			if($rr[0] == $role)
				$ro		.= "<li>" . $rr[1] . "</li>";
		}
	}	
	$ro		.= "</ul>";
	$html 	.= "
	<form id='my_params' class='w-100' >
		<subtitle>" . __("My roles:", BIO) . "</subtitle> 
		<div class='lead'>$ro</div>
		<input type='text'class='form-control' value='" . $user->user_login . "' disabled  />
		<div class='spacer-10'></div>
		<input type='text' name='first_name' class='form-control' value='" . $user->first_name . "' placeholder='" . Bio_Messages::first_name() . "'/>
		<div class='spacer-10'></div>
		<input type='text' name='last_name' class='form-control' value='" . $user->last_name . "' placeholder='" . Bio_Messages::last_name() . "'/>
		<div class='spacer-10'></div>
		<input type='password' name='old_password' class='form-control' value='" . $user->password . "' placeholder='" . Bio_Messages::old_password() . "'/>
		<div class='spacer-10'></div>
		<input type='password' name='password' class='form-control' value='' placeholder='" . Bio_Messages::change_password() . "'/>
		<div class='spacer-10'></div>
	
	</form>";
	$html .= "
			<div>
		<div>";
	
	return $html;
}