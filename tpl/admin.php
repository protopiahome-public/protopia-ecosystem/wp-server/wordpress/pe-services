<?php
	require_once(PESRV_REAL_PATH."tpl/input_file_form.php");
	
	$html = "<style>
			.nav.nav-tabs 
			{
				padding: 0;
			}
		</style>
		<section>
			<div class='container'>
				<div class='row d-flex align-items-center'>
					<div class='col-md-1 critery_cell2'>
						<div class='bio-course-icon-lg' style='background-image:url(" . PESRV_URLPATH . "assets/img/robot.svg);'></div>
					</div>
					<div class='col-md-11 critery_cell2'>
						<div class='display-4'> " . 
							__("Protopia Ecosystem", PESRV) . 
						"</div>
					</div>
				</div>
				<div class='spacer-10'></div>
				<nav>
				  <div class='nav nav-tabs' id='nav-tab' role='tablist'>
					<a class='nav-item nav-link active' id='nav-home-tab' data-toggle='tab' href='#nav-home' role='tab' aria-controls='nav-home' aria-selected='true'>".
						__("Main Settings", PESRV).
					"</a>
					<!--a class='nav-item nav-link' id='nav-settings-tab' data-toggle='tab' href='#nav-settings' role='tab' aria-controls='nav-settings' aria-selected='false'>".
						__("Main menu settings", PESRV).
					"</a-->
					<a class='nav-item nav-link' id='nav-utilities-tab' data-toggle='tab' href='#nav-utilities' role='tab' aria-controls='nav-utilities' aria-selected='false'>".
						__("Utilities", PESRV).
					"</a>
				  </div>
				</nav>
				
				<div class='tab-content' id='nav-tabContent'>
				  <div class='tab-pane fade show active' id='nav-home' role='tabpanel' aria-labelledby='nav-home-tab'>				  
					<ul class='list-group'> 
						<li class='list-group-item '>
							<div class='raw'>
								<div class='col-md-4 col-sm-12'>".
									__("About Page", PESRV).
								"</div>
								<div class='col-md-8 col-sm-12'>".
									wp_dropdown_pages( array(
										'selected'         => static::$options['about_id'],
										'echo'             => false,
										'name'			   => "about_id",
										'ID'			   => "about_id",
										"class"			   => "form-control bio_options",
										"show_option_none" => "-"
									) ) .
									"<div class='spacer-10'></div>
								</div>
							</div>
						</li>
						<li class='list-group-item '>
							<div class='raw'>
								<div class='col-md-4 col-sm-12'>".
									__("Email", PESRV).
								"</div>
								<div class='col-md-8 col-sm-12'>
									<input class='form-control bio_options' name='email' value='" .static::$options['email']. "' />
									<div class='spacer-10'></div>
								</div>
							</div>
						</li>
						<li class='list-group-item '>
							<div class='raw'>
								<div class='col-md-4 col-sm-12'>".
									__("Adress", PESRV).
								"</div>
								<div class='col-md-8 col-sm-12'>
									<input class='form-control bio_options' name='adress' value='" .static::$options['adress']. "' />
									<div class='spacer-10'></div>
								</div>
							</div>
						</li>
						<li class='list-group-item '>
							<div class='raw'>
								<div class='col-md-4 col-sm-12'>".
									__("URL of unique web-client", PESRV).
								"</div>
								<div class='col-md-8 col-sm-12'>
									<input type='url'  class='form-control bio_options' name='web_client_url' value='" .static::$options['web_client_url']. "' 
								</div>
							</div>
						</li> 
						<li class='list-group-item '>
							<div class='raw'>
								<div class='col-md-12 col-sm-12 mb-3 lead'>".
									__("Social links", PESRV).
								"</div>
								<div class='col-md-4 col-sm-12'>".
									__("VK", PESRV).
								"</div>
								<div class='col-md-8 col-sm-12 mb-2'>
									<input class='form-control bio_options mb-2' name='vk' value='" .static::$options['vk']. "' />
								</div>
								<div class='col-md-4 col-sm-12'>".
									__("Youtube", PESRV).
								"</div>
								<div class='col-md-8 col-sm-12 mb-2'>
									<input class='form-control bio_options mb-2' name='youtube' value='" .static::$options['youtube']. "' />
								</div>
								<div class='col-md-4 col-sm-12'>".
									__("Android", PESRV).
								"</div>
								<div class='col-md-8 col-sm-12 mb-2'>
									<input class='form-control bio_options mb-2' name='android' value='" .static::$options['android']. "' />
								</div>
								<div class='col-md-4 col-sm-12'>".
									__("Apple", PESRV).
								"</div>
								<div class='col-md-8 col-sm-12 mb-2'>
									<input class='form-control bio_options mb-2' name='apple' value='" .static::$options['apple']. "' />
								</div>
							</div>
						</li>".
						apply_filters("pesrv_admin", "").
						"		  
				  </div>
				  <div class='tab-pane fade' id='nav-settings' role='tabpanel' aria-labelledby='nav-settings-tab'>				  
					<ul class='list-group'>
						$menu
					</ul>
					<button type='button' class='btn btn-primary save_menu_setting'>" . __("Save all changes", BIO). "</button>
				</div>
				<div class='tab-pane fade' id='nav-utilities' role='tabpanel' aria-labelledby='nav-utilities-tab'>			  
					<ul class='list-group'>
						<li class='list-group-item '> 
						
						</li>
					</ul>
				</div>
				
				<div class='spacer-10'></div>
				 <span id='is_save' style='vertical-align:top; display:inline-block; opacity:0;'>
					<svg aria-hidden='true' viewBox='0 0 512 512' width='40' height='40' xmlns='http://www.w3.org/2000/svg'>
						<path fill='green' d='M435.848 83.466L172.804 346.51l-96.652-96.652c-4.686-4.686-12.284-4.686-16.971 0l-28.284 28.284c-4.686 4.686-4.686 12.284 0 16.971l133.421 133.421c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-28.284-28.284c-4.686-4.686-12.284-4.686-16.97 0z'></path>
					</svg>
				</span>			
				
				
				
			</div>
		</section>";
		echo $html;